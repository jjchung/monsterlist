import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

/**
 * CS20J Program Six (Linked List Exercise) Assignment 6
 * filename: MonsterList.java
 * MonsterList class and Monster Node class
 * Program compiles and is working and tested
 *
 * @author Joseph Chung, materialstuff@live.com, jchung
 * @version 1.0
 */

class MonsterList {
    protected class MonsterNode {
        protected MonsterNode next;
        protected char id;

        /**
         * Constructor for a MonsterNode
         *
         * @param char c the character in id. Must be a alphabetic char
         * @author Joseph Chung
         * @version 1.0
         */

        public MonsterNode(char c) {
            next = null;
            id = c;
        }
    }

    private MonsterNode front;

    /**
     * Constructor for a MonsterList
     *
     * @author Joseph Chung
     * @version 1.0
     */

    public MonsterList() {
        front = null;
    }

    /**
     * Finds the size of the LinkedList
     *
     * @return int size of the LinkedList
     * @author Joseph Chung
     * @version 1.0
     */

    public int getPopulation() {
        int count = 0;
        if (front == null) { return 0; }
        MonsterNode temp = front;
        while (temp != null) {
            count++;
            temp = temp.next;
        }
        return count;
    }

    /**
     * Inserts a MonsterNode to the front of the list
     *
     * @param char c the character in new Node id. Must be a alphabetic char
     * @author Joseph Chung
     * @version 1.0
     */

    public void insertMonster(char c) {
        if (Character.isLetter(c)) {
            MonsterNode temp = new MonsterNode(c);
            temp.next = front;
            front = temp;
        }
    }

    /**
     * Prints the MonsterList
     *
     * @author Joseph Chung
     * @version 1.0
     */

    public void printList() {
        System.out.print("(" + getPopulation() + ") ");
        System.out.print(toString());
    }

    /**
     * Removes all nodes of given char case insensitive
     *
     * @param char c the character id that is to be removed.
     * Must be a alphabetic char
     * @return int number of Nodes that have been removed
     * @author Joseph Chung
     * @version 1.0
     */

    public int removeMonsterType(char c) {
        int count = 0;
        char ch = Character.toLowerCase(c);
        if (front == null) { return count;}
        while (front != null && Character.toLowerCase(front.id) == ch) {
            front = front.next;
            count++;
        }
        if (front == null) {return count; }
        while (front.next != null &&
               Character.toLowerCase(front.next.id) == ch) {
            front.next = front.next.next;
            count++;
        }
        if (front.next != null) {
            MonsterNode prev = front;
            MonsterNode temp = front.next;
            while (temp.next != null) {
                if (Character.toLowerCase(temp.id) == ch) {
                    temp.next = temp.next.next;
                    count++;
                }
                if (temp.next != null) {
                    prev = temp;
                    temp = temp.next;
                }
            }
            if (Character.toLowerCase(temp.id) == ch) {
                prev.next = null;
            }
            prev = front;
            while (prev.next.next != null) {
                prev = prev.next;
            }
            if (Character.toLowerCase(temp.id) == ch) {
                prev.next = null;
            }
        }
        return count;
    }

    /**
     * Adds a MonsterNode to the back of the MonsterList
     *
     * @param char c the character in new Node id. Must be a alphabetic char
     * @author Joseph Chung
     * @version 1.0
     */

    public void insertRear(char c) {
        if (Character.isLetter(c)) {
            MonsterNode temp = new MonsterNode(c);
            if (front == null) {
                front = temp;
            }
            else if (front != null) {
                MonsterNode temp1 = front;
                while (temp1.next != null) {
                    temp1 = temp1.next;
                }
                temp1.next = temp;
            }
        }
    }

    /**
     * Merges two MonsterLists into a given empty list.
     *
     * @param MonsterList a,b the lists that are being merged
     * @author Joseph Chung
     * @version 1.0
     */

    public void merge(MonsterList a, MonsterList b) {
        if (front == null) {
            if (a.front == null && b.front != null) {
                front = b.front;
            }
            else if (b.front == null && a.front != null) {
                front = a.front;
            }
            else if (a.front != null && b.front != null) {
                MonsterNode temp1 = a.front;
                MonsterNode temp2 = b.front;
                while(temp1 != null || temp2 != null) {
                    if (temp1 != null) {
                        insertRear(temp1.id);
                        temp1 = temp1.next;
                    }
                    if (temp2 != null) {
                        insertRear(temp2.id);
                        temp2 = temp2.next;
                    }
                }
                a.front = null;
                b.front = null;
            }
        }
    }

    /**
     * ToString for the MonsterList
     *
     * @author Joseph Chung
     * @version 1.0
     */

    public String toString() {
        StringBuilder str = new StringBuilder();
        if (front == null) { return ""; }
        MonsterNode temp = front;
        while (temp != null) {
            str.append(temp.id).append(" ");
            temp = temp.next;
        }
        return str.toString();
    }

    public static void main(String[] args) {
        MonsterList a = new MonsterList();
        MonsterList b = new MonsterList();
        MonsterList c = new MonsterList();
        String[] arr;
        if (args.length != 0) {
            try {
                String line = "";
                BufferedReader fin =
                    new BufferedReader (new FileReader(args[0]));
                line = fin.readLine();
                while (line != null) {
                    arr = line.split("\\s+");
                    if (arr.length == 3) {
                        if ("0".equals(arr[0])) {
                            if("add".equals(arr[2])) {
                                a.insertMonster(arr[1].charAt(0));
                            }
                            else if ("remove".equals(arr[2])) {
                                a.removeMonsterType(arr[1].charAt(0));
                            }
                        }
                        else if("1".equals(arr[0])) {
                            if("add".equals(arr[2])) {
                                b.insertMonster(arr[1].charAt(0));
                            }
                            else if ("remove".equals(arr[2])) {
                                b.removeMonsterType(arr[1].charAt(0));
                            }
                        }
                    }
                    line = fin.readLine();
                }
                fin.close();
            }
            catch (FileNotFoundException e) {
                System.err.println(args[0] + ": File not found!");
                System.exit(-1);
            }
            catch (IOException e) {
                System.err.println(args[0] + ": Error reading from file!");
                System.exit(-1);
            }
        } else {
            String line;
            Scanner stdin = new Scanner(System.in);
            while (stdin.hasNextLine()) {
                line = stdin.nextLine();
                arr = line.split("\\s+");
                if (arr.length == 3) {
                    if ("0".equals(arr[0])) {
                        if("add".equals(arr[2])) {
                            a.insertMonster(arr[1].charAt(0));
                        }
                        else if ("remove".equals(arr[2])) {
                            a.removeMonsterType(arr[1].charAt(0));
                        }
                    }
                    else if("1".equals(arr[0])) {
                        if("add".equals(arr[2])) {
                            b.insertMonster(arr[1].charAt(0));
                        }
                        else if ("remove".equals(arr[2])) {
                            b.removeMonsterType(arr[1].charAt(0));
                        }
                    }
                }
            }
        }
        System.out.println("CS20j Fall 2018 MonsterList Sample Solution");
        System.out.println("Joseph Chung materialstuff@live.com");
        System.out.println("After reading all input...");
        System.out.print("List 1:");
        a.printList();
        System.out.print("\nList 2:");
        b.printList();
        System.out.println("\nAfter merge...");
        c.merge(a, b);
        System.out.print("List 1:");
        a.printList();
        System.out.print("\nList 2:");
        b.printList();
        System.out.print("\ncombined list:");
        c.printList();
        System.out.println();
    }
}
